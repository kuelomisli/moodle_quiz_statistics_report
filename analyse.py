#!/bin/env python

import json
import re
import xml.etree.ElementTree as ET
import pprint
from openpyxl import load_workbook
import sys

#print("""This program sorts a moodle question analysis file for interesting and problematic questions.
#
#         Usage: analyze statistics.xlsx moodle-quiz.xml
#        """)

if (len(sys.argv) <= 2):
    print("No files specified.")

def update_spreadsheet(names_and_texts, dics):
    # Need to make safe for possible duplicate hits
    match = 0
    for q in dics:
        for p in dics:
            if q['Frage'] == p['Frage']:
                match += 1
    if match != len(dics):
        print("CAREFUL!")
        print("{} question names are duplicates.".format(match - len(dics)))
    match = 0
    for q in dics:
        for tup in names_and_texts:
            if tup[0][:-2] in q['Frage']:
                q['Frage'] = tup[1]
                match += 1
    if match != len(dics):
        print("CAREFUL!")
        print("{} question names matched to texts. \n \
                {} questions in total.".format(match, len(dics)))
    return dics

def parse_xml(root):
    names_and_texts = []

    for child in root.findall('question'):
        question_name = child.find('name').find('text').text
        question_text = child.find('questiontext').find('text').text
        names_and_texts.append((question_name, question_text))
    return names_and_texts

def find_easiest_and_hardest(questions):
    reo = re.compile(r'([-]?\d+.{1}\d+)')

    for q in questions:
        find = re.search(reo, q['Leichtigkeitsindex'])
        find2 = re.search(reo, q['Discrimination Index'])
        #q['Leichtigkeitsindex'] = re.sub(',','.',find.group(1))
        q['Leichtigkeitsindex'] = float(find.group(1).replace(',','.'))
        q['Discrimination Index'] = float(find2.group(1).replace(',','.'))

    sorted_questions = sorted(questions, key=lambda a: a.get('Leichtigkeitsindex'), \
            reverse = True)

    sorted_questions_by_disc = sorted(questions, key=lambda a: a.get('Discrimination Index'))

    print("\n#############################################")
    print("\n Leichteste Fragen: \n") 
    for q in sorted_questions[:3]:
        print("--------------------------------------------")
        print("""{Frage} \nwurde von {Prozent:4.2f}% aller StudentInnen richtig beantwortet.""".format(Frage = q['Frage'], Prozent =q['Leichtigkeitsindex']))

    print("\n#############################################")
    print("\n Schwerste Fragen: \n") 
    for q in sorted_questions[-3:]:
        print("--------------------------------------------")
        print("""{Frage} \nwurde von {Prozent:4.2f}% aller StudentInnen richtig beantwortet.""".format(Frage = q['Frage'], Prozent =q['Leichtigkeitsindex']))
    print("\n#############################################")


    print("\n Kaum unterscheidende Fragen: \n")
    print("""Die folgenden Fragen weisen das geringste unterscheidene Potential auf. Das heißt, dass StudentInnen mit gutem Testergebnis sie nicht unbedingt richtig beantworten, und StudentInnen mit schlechtem Ergebnis sie nicht falsch. Das kann bedeuten, dass diese Fragen anderes Wissen prüfen als die restlichen Fragen.""")
    count = 0
    for q in sorted_questions_by_disc:
        fac = q['Leichtigkeitsindex'] 
        disc = q['Discrimination Index'] 
        if count >= 3:
            break
        if ( fac < 15) or (fac > 85):
            continue
        if disc <= 0:
            continue
        count += 1
        print("--------------------------------------------")
        print("""{Frage} \nDiese Frage hat einen Discrimination Index von {Prozent:4.2f}%.""".format(Frage = q['Frage'], Prozent =q['Discrimination Index']))
    print("\n#############################################")

    negativ_disc_index_found = 0
    for q in sorted_questions:
        if (q['Discrimination Index'] <= 0):    
            negativ_disc_index_found = 1
    if negativ_disc_index_found:
        print("\n Problematische Fragen: \n") 
        print("""Die folgenden Fragen sind problematisch. Sie wurden eher von StudentInnen richtig beantwortet, die auf den Test insgesamt schlecht abgeschnitten haben. Sie wurde von StudentInnen mit gutem Testergebnis eher falsch beantwortet. Das kann beispielsweise bedeuten, dass diese Fragen anderes Wissen prüfen als die restlichen Fragen, oder dass sie unklar formuliert sind.""")
        for q in sorted_questions:
            if (q['Discrimination Index'] >= 0):
                continue
           # if (q['Leichtigkeitsindex'] > 80 or q['Leichtigkeitsindex'] < 20):
           #     continue
            print("--------------------------------------------")
            print("""{Frage} \nDiese Frage hat einen Discrimination Index von {Prozent:4.2f}%.""".format(Frage = q['Frage'], Prozent =q['Discrimination Index']))

def process_xlsx(wb):
    ws = wb.active
# rows is a list of tuples, each tuple a row
    #rows = [x for x in ws.rows ] 
    #categories = [x.value for x in rows[0]]

    rows = ws.rows
    categories = [x.value for x in next(rows)]

    dics = []
    for part_list in rows:
        question_dic = dict().fromkeys(categories)
        #question_dic.update(categories, part_list)
        for q,p in zip(question_dic, part_list):
            question_dic[q] = p.value

        dics.append(question_dic)
        #for category, item in zip(categories, part_list):
            #question_dic.
    return dics

def process_json(full_list):

    print(full_list[0])

    categories = ["F#","Typ der Frage","Frage", "Ergebnisse", "Leichtigkeitsindex","Standardabweichung","Beabsichtigte Gewichtung","Effektive Gewichtung","Discrimination Index","Richtig beantwortet","Teilweise richtig beantwortet", "Falsch beantwortet"]

    dics = []
    for part_list in full_list:
        question_dic = dict().fromkeys(categories)
        #question_dic.update(categories, part_list)
        for q,p in zip(question_dic, part_list):
            question_dic[q] = p

        dics.append(question_dic)
        #for category, item in zip(categories, part_list):
            #question_dic.
    return dics

def main(): 
    with open(sys.argv[1], 'r') as f:
        if "json" in sys.argv[1][-5:]:
            print("json detected.")
            full_list = json.load(f)
            full_list = full_list[0]
            dics = process_json(full_list)

        if "xlsx" in sys.argv[1][-5:]:
            print("Excel Spreadsheet detected.")
            wb = load_workbook(sys.argv[1])
            dics = process_xlsx(wb)

    tree = ET.parse(sys.argv[2])
    root = tree.getroot()

    names_and_texts = parse_xml(root)
    dics = update_spreadsheet(names_and_texts, dics)

    find_easiest_and_hardest(dics)

main()


